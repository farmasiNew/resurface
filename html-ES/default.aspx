﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LandingPage_Default" %>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>VFX Pro Fondöten</title>
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
    <link rel="preconnect" href="https://cdnjs.cloudflare.com">
    <link rel="preconnect" href="https://code.jquery.com/">
    <link rel="preconnect" href="https://cdn.jsdelivr.net/">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/vfxprofondoten/css/site.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/vfxprofondoten/img/favicon/favicon.png" />
    <link rel="apple-touch-icon" href="/vfxprofondoten/img/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/vfxprofondoten/img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/vfxprofondoten/img/favicon/apple-touch-icon-114x114.png" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162929067-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', 'UA-162929067-2');
    </script>
  </head>

  <body>
    <header id="frmtHead">
      <div class="container">
        <div class="row">
          <div class="col-4">
          </div>
          <div class="col-4 text-center">
            <a href="/">
              <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/farmasi-rednwhite.png"
                class="lazyload img-fluid" alt="VFX Pro Fondöten" width="170" height="40">
            </a>
          </div>
          <div class="col-4 text-right">
            <button id="menu-btn" onclick="menuOpen()">
              <svg width="24" height="24" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 24H42" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M6 12H42" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M6 36H42" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </header>
    <div id="menu">
      <button class="close-menu" onclick="menuClose()">
        <svg style="width: 24px; height: 24px" viewBox="0 0 24 24">
          <path fill="#fff"
            d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z">
          </path>
        </svg>
      </button>
      <ul id="menuList">
        <li><a href="#section1" class="scroll">Yeni VFX Pro Fondöten</a></li>
        <li><a href="#section2" class="scroll">10 Renk Seçeneği</a></li>
        <li><a href="#video" class="scroll">Yeni VFX Pro Foundation</a></li>
        <li><a href="#section4" class="scroll">VFX Pro'dan Önce - Sonra</a></li>
        <li><a href="#section5" class="scroll">VFX Pro Camera Ready</a></li>
        <li><a href="#section6" class="scroll">VFX Pro Fondöten Renkleri</a></li>
        <li><a href="#section9" class="scroll">Cilt Alt Tonları</a></li>
      </ul>
    </div>
    <section id="section1">
      <a href="#">
        <picture>
          <source media="(max-width: 1023px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/001-m.jpg" --small>
          <source media="(min-width: 1024px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/001.jpg" --medium>
          <img srcset="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/001.jpg" alt="VFX Pro Fondöten"
            class="lazyload">
        </picture>
      </a>
    </section>
    <section id="section2">
      <a href="#">
        <picture>
          <source media="(max-width: 1023px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/002-m.jpg" --small>
          <source media="(min-width: 1024px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/002.jpg" --medium>
          <img srcset="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/002.jpg" alt="VFX Pro Fondöten"
            class="lazyload">
        </picture>
      </a>
    </section>
    <div id="video" class="jarallax">
      <div class="vid-container">
        <iframe width="560" height="315" data-src="https://www.youtube.com/embed/c5G-AFdjV80?rel=0" frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen="" class="lazyload"></iframe>
      </div>
    </div>
    <section id="section4">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/004.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <section id="section5">
      <a href="#">
        <picture>
          <source media="(max-width: 1023px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/005-m.jpg" --small>
          <source media="(min-width: 1024px)" srcset="/vfxprofondoten/img/blank.png"
            data-srcset="/vfxprofondoten/img/005.jpg" --medium>
          <img srcset="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/005.jpg" alt="VFX Pro Fondöten"
            class="lazyload">
        </picture>
      </a>
    </section>
    <section id="section6">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/006.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <section id="section7">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/007.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <section id="section8">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/008.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <section id="section9">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/009.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <section id="section10">
      <a href="#">
        <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/010.jpg" alt="VFX Pro Fondöten"
          class="lazyload">
      </a>
    </section>
    <footer id="frmFooter">
      <ul>
        <li>
          <a href="https://www.facebook.com/FarmasiTurkeyOfficial/" target="_blank" rel="noreferrer">
            <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/facebook.png" alt="VFX Pro Fondöten"
              class="lazyload">
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/farmasiofficial/" target="_blank" rel="noreferrer">
            <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/instagram.png" alt="VFX Pro Fondöten"
              class="lazyload instagram">
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/channel/UCBT7TowgH5y9zM6lPtZadMg?view_as=subscriber" target="_blank"
            rel="noreferrer">
            <img src="/vfxprofondoten/img/blank.png" data-src="/vfxprofondoten/img/youtube.png" alt="VFX Pro Fondöten"
              class="lazyload">
          </a>
        </li>
      </ul>
      <p>© 2021 Farmasi</p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.3.2/lazysizes.min.js"
      integrity="sha512-q583ppKrCRc7N5O0n2nzUiJ+suUv7Et1JGels4bXOaMFQcamPk9HjdUknZuuFjBNs7tsMuadge5k9RzdmO+1GQ=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.12.7/jarallax.min.js"
      integrity="sha512-JpiSiPEgPmeTAnXyH375a9UxAsjoiyO+lpsayQd7Pcl2+tYf4y1beIF25yDMvmAV7dbZUPqh084iT3D6IQOoHg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="/vfxprofondoten/js/core.min.js"></script>
  </body>

  </html>